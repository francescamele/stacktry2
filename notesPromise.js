/**
 * Devono essere sottoposte a transpiling, perché sono ES6
 * 
 * 
 */
//Se pensi a:
const a = 10;

function doppio() {
    return a * 2;
}

doppio();

const b = 'ciaoo';
//Il JS engine deve andare da riga 13 a riga 7, poi saltare di nuovo a 15.
//Ogni volta che faccio questa cosa, 1h31

const a = 10;

function doppio() {
    return a * 2;
}

//Voglio che qui ci sia una funz matematica molto complessa che non blocchi
//lo script principale: dico di eseguirla dopo.
//Usa, per farlo, l'Event Loop: questo si occupa di controllare che nello
//stack non ci sia nulla. Quando si svuota, controlla la Task Queue
const p = new Promise((resolve, reject) => {
    //Quello che dovrà fare la Pr
    //Questa funzione è una callback: è la Pr che decide quando eseguirla
    //Per risolvere una Pr:
    return resolve(30);

    //Se devo andare in errore:
    return reject("c'è stato un errore");
})

//Come ascolto la Promise?
//p è la Pr, then è l'oggetto che mi permette di agganciarmi al risultato
//della Pr
// p.then(
//     //Funzione da eseguire in caso di successo:
//     (risultato) => console.log(risultato)
//     //Funzione da eseguire in caso di errore
//     (errore) => console.error(errore)
// );
//O sennò:
p.then(
    //Funzione da eseguire in caso di successo:
    (risultato) => risultato * 2,
    //Funzione da eseguire in caso di errore
    (errore) => console.error(errore)
).then((altroRisultato) => console.log(altroRisultato));
//1h48'

doppio();

const b = 'ciaoo';