/**
 * module.exports è la stessa identica cosa che facciamo con 
 * export default {}
 * import config from '...'
 * 
 * Però qui usiamo la sintassi di nodeJS.
 * Serve solo ad esportare un oggetto, non devi saper scrivere in questo 
 * modo. Però devi riuscire ad analizzare un codice pur non conodcendo 
 * tutto quello che dice: è importante capire grosso modo la ragion d'essere 
 * di quel codice
 */
//Qui dentro dobbiamo declinare tutte le regole di configurazione per webpack
module.exports = {
    //Il file da cui parte la nostra applicazione:
    //entry: './src/index.js',
    //C'è anche un altro approccio: entry è un oggetto, al cui interno 
    //definisco con una chiave il nome di UN output e con un valore 
    //corrispondente metto il file che dovrà analizzare
    //Comodo quando devo gestire più bundle allo stesso tempo
    entry : {
        main: './src/main.js',
        //altroIngresso: ''
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            //test dice a quali file saranno sottoposti a questa regola
            test: /\.js$/,
            use: [
                //Questo array rimarrà vuoto finché non installi prettier,
                // babel, o altri loader
                'babel-loader',
                'prettier-loader'
            ]
        }]
    },
    output: {
        //Qual è il filename? Se c'è un solo punto d'ingresso, sarà sempre 
        //bundle.js. Ma se abbiamo diversi entry point, aggiungi [name] per
        //fargli leggere la chiave scritta in entry, così la aggiunge al
        //nome del bundle. Altrimenti, ogni volta che creerà il bundle 
        //sovrascriverà quello creato in precedenza.
        filename: '[name]bundle.js',
        //^Quando trova queste quadre, webpack si troverà il nome del file 
        //js da cui parte e creerà un file che inizia con quel nome. Quindi 
        //in questo caso dell'esempio commentato, o main.bundle.js o 
        //altroIngresso.bundle.js
        //Se non metti [name], ti creerà un bundle.js ogni volta e quindi
        //sovrascriverà qualsiasi cosa abbia fatto prima.
        //Il nome del file che crea webpack dipende dal nome della chiave 
        //in entry.
        //Prima info: __dirname serve a wbp, anzi a nodeJS, per capire il 
        //percorso dalla cartella principale del nostro disco rigido fino 
        //a questa cartella dove si trova il file webp: gli serve il  
        //percorso completo. Quindi ci serve ad arrivare qui dentro, dove 
        //si trova il file webpack.config.js. In più, dobbiamo dirgli dove 
        //vadano messi questi file: nella cartella 'dist', che verrà creata 
        //in automatico, parallelamente a 'node_modules'
        path: __dirname + '/dist'
    }
}